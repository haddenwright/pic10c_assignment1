



# What is this? #
* This is a program for a Spanish Card game known as siete y medio.
* This is the first assignment for the class at ucla titled PIC 10 C. 
* The intent of this assignment was to become familiar with using version control software. 
* This repository contains multiple files with multiple commits on multiple different branches that are then merged (both with fast forward and non-fast forward merges).

# Things worth noting: #
* This program is incomplete and does not compile.

# How to play the game: #
*  The goal of the game is to get cards whose total value comes the closest to 71/2 without going over it. Getting a card total over 71/2 is called "busting". When a player makes a bet against the dealer. There are 4 possible outcomes: The player comes closer to 71/2 than the dealer or the dealer busts but the player did not bust. In this case the player wins the round and the player's money increases by the amount that was bet. The dealer comes closer to 71/2 than the player, or the player busts. In this case the player loses the round and the player's money decreases by the amount that was bet. Both, the player and dealer bust. In this case the player loses the round and the player's money decreases by the amount that was bet. This is called house advantage. Both the player and the dealer have the same total and  they do not bust. In this case a tie is declared and no money is exchanged.

# Example #

* Game:  

You have $100. Enter bet: 99
Your cards:
	Dos de copas        (Two of cups).
Your total is 2. Do you want another card (y/n)? n
Dealer's cards:	Sota de espadas     (Jack of spades).
The dealer's total is 0.5.

New card:
	Rey de espadas (King of spades).

Dealer's cards:
	Sota de espadas     (Jack of spades).
	Rey de espadas      (King of spades).
The dealer's total is 1.

New card:
	Siete de bastos (Seven of clubs).

Dealer's cards:
	Siete de bastos     (Seven of clubs).
	Sota de espadas     (Jack of spades).
	Rey de espadas      (King of spades).
The dealer's total is 8.

You win 99.

You have $199. Enter bet: 198
Your cards:
	Caballo de bastos   (Knight of clubs).
Your total is 0.5. Do you want another card (y/n)? y
New card:
	As de espadas (Ace of spades).

Your cards:
	As de espadas       (Ace of spades).
	Caballo de bastos   (Knight of clubs).
Your total is 1.5. Do you want another card (y/n)? y
New card:
	Cuatro de espadas (Four of spades).

Your cards:
	As de espadas       (Ace of spades).
	Cuatro de espadas   (Four of spades).
	Caballo de bastos   (Knight of clubs).
Your total is 5.5. Do you want another card (y/n)? n
Dealer's cards:	Rey de bastos       (King of clubs).
The dealer's total is 0.5.

New card:
	Seis de bastos (Six of clubs).

Dealer's cards:
	Seis de bastos      (Six of clubs).
	Rey de bastos       (King of clubs).
The dealer's total is 6.5.

Too bad. You lose 198.

You have $1. Enter bet: 1
Your cards:
	Sota de copas       (Jack of cups).
Your total is 0.5. Do you want another card (y/n)? y
New card:
	Tres de bastos (Three of clubs).

Your cards:
	Tres de bastos      (Three of clubs).
	Sota de copas       (Jack of cups).
Your total is 3.5. Do you want another card (y/n)? y
New card:
	Cuatro de copas (Four of cups).

Your cards:
	Tres de bastos      (Three of clubs).
	Cuatro de copas     (Four of cups).
	Sota de copas       (Jack of cups).
Your total is 7.5. Do you want another card (y/n)? n
Dealer's cards:	Siete de copas      (Seven of cups).
The dealer's total is 7.

You win 1.

You have $2. Enter bet: 2
Your cards:
	Cinco de espadas    (Five of spades).
Your total is 5. Do you want another card (y/n)? n
Dealer's cards:	As de bastos        (Ace of clubs).
The dealer's total is 1.

New card:
	Siete de copas (Seven of cups).

Dealer's cards:
	As de bastos        (Ace of clubs).
	Siete de copas      (Seven of cups).
The dealer's total is 8.

You win 2.

You have $4. Enter bet: 4
Your cards:
	As de espadas       (Ace of spades).
Your total is 1. Do you want another card (y/n)? y
New card:
	Seis de copas (Six of cups).

Your cards:
	As de espadas       (Ace of spades).
	Seis de copas       (Six of cups).
Your total is 7. Do you want another card (y/n)? n
Dealer's cards:	Dos de oros         (Two of coins).
The dealer's total is 2.

New card:
	Sota de oros (Jack of coins).

Dealer's cards:
	Dos de oros         (Two of coins).
	Sota de oros        (Jack of coins).
The dealer's total is 2.5.

New card:
	Seis de oros (Six of coins).

Dealer's cards:
	Dos de oros         (Two of coins).
	Seis de oros        (Six of coins).
	Sota de oros        (Jack of coins).
The dealer's total is 8.5.

You win 4.

You have $8. Enter bet: 8
Your cards:
	Seis de oros        (Six of coins).
Your total is 6. Do you want another card (y/n)? n
Dealer's cards:	Seis de espadas     (Six of spades).
The dealer's total is 6.

Nobody wins!

You have $8. Enter bet: 8
Your cards:
	As de bastos        (Ace of clubs).
Your total is 1. Do you want another card (y/n)? y
New card:
	Caballo de oros (Knight of coins).

Your cards:
	As de bastos        (Ace of clubs).
	Caballo de oros     (Knight of coins).
Your total is 1.5. Do you want another card (y/n)? y
New card:
	Cinco de oros (Five of coins).

Your cards:
	As de bastos        (Ace of clubs).
	Cinco de oros       (Five of coins).
	Caballo de oros     (Knight of coins).
Your total is 6.5. Do you want another card (y/n)? n
Dealer's cards:	Caballo de bastos   (Knight of clubs).
The dealer's total is 0.5.

New card:
	Dos de bastos (Two of clubs).

Dealer's cards:
	Dos de bastos       (Two of clubs).
	Caballo de bastos   (Knight of clubs).
The dealer's total is 2.5.

New card:
	Rey de espadas (King of spades).

Dealer's cards:
	Dos de bastos       (Two of clubs).
	Caballo de bastos   (Knight of clubs).
	Rey de espadas      (King of spades).
The dealer's total is 3.

New card:
	As de oros (Ace of coins).

Dealer's cards:
	As de oros          (Ace of coins).
	Dos de bastos       (Two of clubs).
	Caballo de bastos   (Knight of clubs).
	Rey de espadas      (King of spades).
The dealer's total is 4.

New card:
	Caballo de oros (Knight of coins).

Dealer's cards:
	As de oros          (Ace of coins).
	Dos de bastos       (Two of clubs).
	Caballo de bastos   (Knight of clubs).
	Caballo de oros     (Knight of coins).
	Rey de espadas      (King of spades).
The dealer's total is 4.5.

New card:
	Tres de bastos (Three of clubs).

Dealer's cards:
	As de oros          (Ace of coins).
	Dos de bastos       (Two of clubs).
	Tres de bastos      (Three of clubs).
	Caballo de bastos   (Knight of clubs).
	Caballo de oros     (Knight of coins).
	Rey de espadas      (King of spades).
The dealer's total is 7.5.

Too bad. You lose 8.

You have $0. GAME OVER!
Come back when you have more money.

Bye!  

* Log:  

-----------------------------------------------

Game number: 1		Money left: $100
Bet: 99

Your cards:
	Dos de copas        (Two of cups).
Your total: 2.

Dealer's cards:
	Siete de bastos     (Seven of clubs).
	Sota de espadas     (Jack of spades).
	Rey de espadas      (King of spades).
Dealer's total is 8.

-----------------------------------------------

Game number: 2		Money left: $199
Bet: 198

Your cards:
	As de espadas       (Ace of spades).
	Cuatro de espadas   (Four of spades).
	Caballo de bastos   (Knight of clubs).
Your total: 5.5.

Dealer's cards:
	Seis de bastos      (Six of clubs).
	Rey de bastos       (King of clubs).
Dealer's total is 6.5.

-----------------------------------------------

Game number: 3		Money left: $1
Bet: 1

Your cards:
	Tres de bastos      (Three of clubs).
	Cuatro de copas     (Four of cups).
	Sota de copas       (Jack of cups).
Your total: 7.5.

Dealer's cards:
	Siete de copas      (Seven of cups).
Dealer's total is 7.

-----------------------------------------------

Game number: 4		Money left: $2
Bet: 2

Your cards:
	Cinco de espadas    (Five of spades).
Your total: 5.

Dealer's cards:
	As de bastos        (Ace of clubs).
	Siete de copas      (Seven of cups).
Dealer's total is 8.

-----------------------------------------------

Game number: 5		Money left: $4
Bet: 4

Your cards:
	As de espadas       (Ace of spades).
	Seis de copas       (Six of cups).
Your total: 7.

Dealer's cards:
	Dos de oros         (Two of coins).
	Seis de oros        (Six of coins).
	Sota de oros        (Jack of coins).
Dealer's total is 8.5.

-----------------------------------------------

Game number: 6		Money left: $8
Bet: 8

Your cards:
	Seis de oros        (Six of coins).
Your total: 6.

Dealer's cards:
	Seis de espadas     (Six of spades).
Dealer's total is 6.

-----------------------------------------------

Game number: 7		Money left: $8
Bet: 8

Your cards:
	As de bastos        (Ace of clubs).
	Cinco de oros       (Five of coins).
	Caballo de oros     (Knight of coins).
Your total: 6.5.

Dealer's cards:
	As de oros          (Ace of coins).
	Dos de bastos       (Two of clubs).
	Tres de bastos      (Three of clubs).
	Caballo de bastos   (Knight of clubs).
	Caballo de oros     (Knight of coins).
	Rey de espadas      (King of spades).
Dealer's total is 7.5.

-----------------------------------------------